## Hi there 👋
 
I am a front-end developer, mainly developing in __Vue__ and __Angular__, but I also try to learn other technologies.

### About me
- Studied at [ITMO University](https://en.itmo.ru/)
- Make responsive websites and mobile apps
- Love complex tasks and Linux Ubuntu 💻

### Contact me 📫
  - __Telegram__: [lubarog13](https://telegram.me/lubarog13)
  - __Gmail__: [lubarog13@gmail.com](mailto:lubarog13@gmail.com) 
  - __Linkedin__: www.linkedin.com/in/lubarog13
